package com.kvtsoft.springnavbar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringNavbarApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringNavbarApplication.class, args);
    }
}
