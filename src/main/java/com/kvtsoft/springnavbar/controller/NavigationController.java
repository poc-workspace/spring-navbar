package com.kvtsoft.springnavbar.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

@Controller
public class NavigationController {

    @GetMapping("/info")
    public String info(Model model) {
        model.addAttribute("activePage", "info");
        return "info";
    }

    @GetMapping("/contact")
    public String contact(Model model) {
        model.addAttribute("activePage", "contact");
        return "contact";
    }
}
